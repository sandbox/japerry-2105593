<?php

/**
 * @file
 * Project Browser module admin pages.
 */

/**
 * Form constructor for the admin settings form.
 *
 * @see project_browser_menu()
 * @see project_browser_admin_form_submit()
 * @ingroup forms
 */
function project_browser_admin_form($form, &$form_state) {
  $form['main'] = array(
    '#type' => 'details',
    '#title' => t('Main settings'),
    '#collapsed' => FALSE,
  );
  // Because this is a pluggable system, there can be other repositories besides
  // Drupal.org.
  $form['main']['server_list'] = array(
    '#type' => 'textarea',
    '#title' => t('Repositories'),
    '#default_value' => config('project_browser.settings')->get('server.list'),
    '#description' => t("Add new repositories to use for the Project Browser, one per line, in the 'url|method|Site Name' format. Drupal.org is added by default, and doesn't need to be set here."),
    '#required' => FALSE,
  );

  return system_config_form($form, $form_state);
}

/**
 * Form submission handler for project_browser_admin_form().
 */
function project_browser_admin_form_submit($form, &$form_state) {
  $config = config('project_browser.settings');
  $config->set('server.list', $form_state['values']['server_list']);
  $config->save();
}
