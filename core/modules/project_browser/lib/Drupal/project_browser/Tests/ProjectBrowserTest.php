<?php

/**
 * @file
 * Contains \Drupal\project_browser\Tests\ProjectBrowserTest.
 */

namespace Drupal\project_browser\Tests;

use Drupal\simpletest\WebTestBase;

/**
 * Administration tests for Project Browser.
 */
class ProjectBrowserTest extends WebTestBase {

  /**
   * Modules to enable.
   *
   * @var array
   */
  public static $modules = array('project_browser', 'project_browser_test');

  /**
   * User who will have access to use project browser.
   *
   * @var \Drupal\user\Plugin\Core\Entity\User
   */
  protected $privilegedUser;

  public static function getInfo() {
    return array(
      'name' => 'Project Browser Install Project Test',
      'description' => 'Attempts to install a project.',
      'group' => 'Project Browser',
    );
  }

  function setUp() {
    parent::setUp();

    // Set the default server variable.
    $server_url = url('project_browser_test/query', array('absolute' => TRUE));
    $config = config('project_browser.settings');
    $config->set('server.default', array(
        'name' => 'Test Server',
        'method' => 'json',
        'url' => $server_url,
    ));
    $config->save();

    // Create and log in our privileged user.
    $this->privilegedUser = $this->drupalCreateUser(array(
      'use project browser',
    ));
    $this->drupalLogin($this->privilegedUser);
  }

  /**
   * Tests a search for the Token string, ensuring that token shows.
   */
  public function testProjectBrowserSearchToken() {
    // Search for 'token'.
    $edit = array();
    $edit['search_text'] = 'token';
    $this->drupalPost('admin/modules/project-browser/modules', $edit, t('Filter'));
    $this->assertResponse(200);
    $this->assertText('Showing 1 to');
    $this->assertText('Tokens are small bits of text');
  }

  /**
   * Tests that the Projects Listing displays properly with the default filters.
   */
  public function testProjectBrowserGetProjects() {
    // Attempt to fetch the default projects.
    $edit = array();
    $edit['search_text'] = '';
    $this->drupalPost('admin/modules/project-browser/modules', $edit, t('Filter'));
    $this->assertResponse(200);
    $this->assertText('Showing 1 to');
  }

  /**
   * Tests to make sure that the enabled project detection works.
   */
  public function testProjectBrowserProjectEnabled() {
    // Make sure project enabled detection works.
    module_load_include('inc', 'project_browser', 'project_browser');
    $this->assertTrue(_project_browser_is_project_enabled('module', 'project_browser'), 'Make sure project enabled detection works.');
  }

  /**
   * Tests that adding and removing items from the queue works properly.
   */
  public function testProjectBrowserAddRemoveQueue() {
    // Refresh the page.
    $this->drupalGet('admin/modules/project-browser/modules');

    // Simulate adding a project to the install queue.
    $this->drupalGet('project-browser/nojs/install-queue/add/views', array('query' => array('destination' => 'admin/modules/project-browser')));
    $this->assertNoText('Install queue is empty.');
    $this->assertNoText('Error: The project was not found.');

    // Simulate removing a project from the install queue.
    $this->drupalGet('project-browser/nojs/install-queue/remove/views', array('query' => array('destination' => 'admin/modules/project-browser')));
    $this->assertText('Install queue is empty.');
    $this->assertNoText('Error: The project was not found.');
  }

}
