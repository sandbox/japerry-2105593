<?php

namespace Drupal\project_browser;

use Drupal\Core\Config\Config;

class ProjectBrowserBase {

  protected $projects = NULL;
  protected $projectType = NULL;

  /**
   * The system.theme config object.
   *
   * @var \Drupal\Core\Config\Config
   */
  protected $config;
  /**
   * Constructs a ThemeController object.
   *
   * @param \Drupal\Core\Config\Config $config
   *   The config.
   */
  public function __construct(Config $config) {
    $this->config = $config;
  }

  /**
   * Builds an array of all available categories for a project type.
   *
   * @param string $type
   *   The type of project to get the categories for. Example: 'module' or
   *   'theme'.
   *
   * @return array|null
   *   Array containing all available categories or NULL if no categories.
   */
  function getCategories() {
    $categories = array();

    // Get which server to use from $_SESSION.
    $use_server = isset($_SESSION['project_browser_server_filter']) ? $_SESSION['project_browser_server_filter'] : 0;

    $categories_raw = $this->fetchCategories($use_server);

    if (is_array($categories_raw) && !empty($categories_raw)) {
      foreach ($categories_raw as $url => $cats) {
        foreach ($cats as $key => $value) {
          // Create a new key so that there are no duplicate categories from
          // different sites.
          $new_key = preg_replace('/[^a-z0-9_]+/', '_', strtolower($value));
          $categories[$new_key] = $value;
        }
      }
    }

    if (is_array($categories) && !empty($categories)) {
      ksort($categories);

      return $categories;
    }
    return NULL;
  }

  /**
   * Fetches categories from the servers based on the type of project.
   *
   * @param string $type
   *   The type of project we are getting categories for, a string, which can be
   *   'module' or 'theme'.
   * @param string $use_server
   *   (optional) The server to use, which is a string. Defaults to 'all'.
   *
   * @return array
   *   Returns an array of the categories.
   */
  protected function fetchCategories($use_server = 'all') {
    $servers = $this->getServers($use_server);

    // Attempt to retrieve the cached version of this page.
    $cid = md5('categories_' . $this->projectType . serialize($servers));

    if ($cache = cache()->get($cid)) {
      return $cache->data;
    }
    else {
      $categories = array();

      foreach ($servers as $url => $server) {
        $categories_raw = array();
        // Use xmlrpc if it is set.
        if ($server['method'] == 'xmlrpc') {
          $categories_raw = xmlrpc($url, array(
            'project_browser_server.fetch_categories' => array($this->projectType),
          ));

          // Check for errors.
          if ($error = xmlrpc_error() && $error->is_error) {
            drupal_set_message(t("Encountered an error when trying to fetch categories from @name. Error @code : @message",
              array('@name' => $server['name'], '@code' => $error->code, '@message' => $error->message)));
            continue;
          }
        }

        // Use json if it is set.
        if ($server['method'] == 'json') {
          $params = array(
            'method' => 'categories',
            'type' => $this->projectType,
          );

          $request = \Drupal::httpClient()->get($url . '/categories/' . $this->projectType . '?' . http_build_query($params, FALSE, '&'));

          try {
            $response = $request->send();
            // Expected result.
            $categories_raw = $json = Json::decode($response->getBody(TRUE));
          }
          catch (\Exception $e) {
            watchdog_exception('project_browser', $e);
          }

          /*
          else {
            drupal_set_message(t("Encountered an error when trying to fetch categories from @name. Error @code : @message",
              array('@name' => $server['name'], '@code' => $response->code, '@message' => $response->error)));
            continue;
          }
          */
        }

        if (is_array($categories_raw) && !empty($categories_raw)) {
          $categories[$url] = $categories_raw;
        }
      }

      // Cache this for 24 hours.
      cache()->set($cid, $categories, REQUEST_TIME + $this->config->get('cache_lifetime'));
    }

    return $categories;
  }

  /**
   * Gets an array of servers to use for fetching results.
   *
   * @param string $use_server
   *   (optional) The server to use, which is a string. Defaults to 'all'.
   *
   * @return array
   *   Returns an associative array of servers, populated from the
   *   project_browser_servers variable, in 'url => name' format.
   */
  protected function getServers($use_server = 'all') {
    $default_server = $this->config->get('server.default');
    $url = $default_server['url'];
    unset($default_server['url']);
    $servers[$url] = $default_server;
    if ($servers_raw = $this->config->get('server.list')) {
      // Process the variable and add the servers to the list.
      $custom_servers = array();

      $list = explode("\n", $servers_raw);
      $list = array_map('trim', $list);
      $list = array_filter($list, 'strlen');

      foreach ($list as $position => $text) {
        $method = $name = $url = FALSE;

        $matches = array();
        if (preg_match('/(.*)\|(.*)\|(.*)/', $text, $matches)) {
          $url = $matches[1];
          $method = $matches[2];
          $name = $matches[3];
          $custom_servers[$url] = array('name' => $name, 'method' => $method);
        }
      }

      $servers = array_merge($servers, $custom_servers);
    }

    // Filter out servers if necessary.
    // @todo Improve comment or variable name of $i.
    // Iterate through the server number.
    if ($use_server !== 'all') {
      $i = 0;
      foreach ($servers as $url => $server) {
        if ($use_server != $i) {
          unset($servers[$url]);
        }
        $i += 1;
      }
    }

    return $servers;
  }


  /**
   * Prepares the categories for sending to the servers as filters.
   *
   * @param array $raw_cats
   *   An array of categories from $form_state['values'].
   * @param string $type
   *   The type of project for which to prepare the categories, as a string, for
   *   example, string 'module' or 'theme'.
   *
   * @return array
   *   An array of server categories, keyed by server url.
   */
  protected function prepareCategories(array $raw_cats) {
    $categories = $this->fetchCategories($this->projectType);

    // Set the value of the categories to true if it is selected.
    foreach ($categories as $url => $cats) {
      foreach ($cats as $key => $value) {
        $new_key = preg_replace('/[^a-z0-9_]+/', '_', strtolower($value));
        if (isset($raw_cats[$new_key]) && $raw_cats[$new_key]) {
          $categories[$url][$key] = TRUE;
        }
        else {
          unset($categories[$url][$key]);
        }
      }

      // Unset the parent if there are no children.
      if (empty($categories[$url])) {
        unset($categories[$url]);
      }
    }

    return $categories;
  }

  /**
   * Fetches results from the servers based on the parameters passed in.
   *
   * @param array $filters
   *   An associative array of queries to use to filter results, containing:
   *   - version: The Major Version of Drupal that is running on the Client.
   *     Example: '7' or '8'.
   *   - text: The text that was entered as the search query, or '' if none.
   *     Example: 'Link field'.
   *   - categories: An array of categories that were selected, if any.
   *   - type: The type of project being searched for. Example: 'module' or
   *     'theme'.
   *   - page: The zero-based page number.
   *   - requested: How many results are requested per page.
   *
   * @return array
   *   An array of results formatted like:
   *   - total: The total number of results found for the filters.
   *   - projects: An array of projects returned for this page request,
   *     containing:
   *     - KEY: A project array keyed by the machine name:
   *       - type: The type of project this is. Can be 'module' or 'theme'.
   *       - title: The title of the project.
   *       - name: The machine name of the project.
   *       - author: The author's name.
   *       - description: Long project description text.
   *       - image: Absolute url to the image, if any.
   *       - usage: How many sites are using module.
   *       - project url: Absolute url to the project page, if any.
   *       - project status url: The absolute url of the update checker,
   *         formatted like how Drupal.org Update Status does it.
   *       - last updated: UNIX Timestamp of when the project was last updated.
   *       - maintenance status: Maintenance status.
   *       - development status: Development status.
   *       - rating: A rating on a scale of 1 to 10 of the project, if available
   *       - dependencies: An array of the dependencies of this module, by
   *         project shortname, if available.
   */
  protected function fetchResults(array $filters) {
    $servers = $this->getServers($filters['server']);

    // Attempt to retrieve the cached version of this page.
    $cid = md5(serialize(array_merge($filters, $servers)));

    if ($cache = cache()->get($cid)) {
      return $cache->data;
    }

    $results = array(
      'projects' => array(),
      'total' => 0,
    );

    unset($filters['server']);

    foreach ($servers as $url => $server) {
      $local_filters = $filters;

      // We are not using this right now because we only expect to handle 1
      // server at a time currently.
      // $local_filters['requested'] = floor($filters['requested'] / count($servers));

      // Send only the relevant categories to the server.
      if (isset($filters['categories'])) {
        if (!isset($filters['categories'][$url])) {
          // Don't call a server for results if categories are being used, and
          // none of them belong to the server.
          continue;
        }
        $local_filters['categories'] = $filters['categories'][$url];
      }

      // Use XMLRPC if it is set.
      if ($server['method'] == 'xmlrpc') {
        $results_raw = xmlrpc($url, array(
          'project_browser_server.fetch_results' => array($local_filters),
        ));

        // Check for errors.
        if ($error = xmlrpc_error() && $error->is_error) {
          drupal_set_message(t("Encountered an error when trying to fetch results from @name. Error @code : @message",
            array('@name' => $server['name'], '@code' => $error->code, '@message' => $error->message)));
          continue;
        }
      }

      // Use json if it is set.
      if ($server['method'] == 'json') {
        $local_filters['method'] = 'query';
        if (isset($local_filters['categories'])) {
          $local_filters['categories'] = serialize($local_filters['categories']);
        }

        $query_url = $url . '/query/' . $local_filters['type'] . '/8?' . http_build_query($local_filters, FALSE, '&');
        dpm($query_url);
        $request = \Drupal::httpClient()->get($query_url);

        try {
          dpm("after request");
          $response = $request->send();
          dpm("after send");
          // Expected result.
          $results_raw = $json = Json::decode($response->getBody(TRUE));
        }
        catch (\Exception $e) {
          dpm("request exeception");
          watchdog_exception('project_browser', $e);
        }
        dpm("below try");
        /*
        else {
          drupal_set_message(t("Encountered an error when trying to fetch results from @name. Error @code : @message",
            array('@name' => $server['name'], '@code' => $response->code, '@message' => $response->error)));
          continue;
        }
        */
      }

      if (isset($results_raw['total'])) {
        $results['total'] += $results_raw['total'];
      }

      if (!empty($results_raw['projects']) && is_array($results_raw['projects'])) {
        // Merge the results.
        $results['projects'] = array_merge($results['projects'], $results_raw['projects']);
      }
    }

    cache()->set($cid, $results, REQUEST_TIME + $this->config->get('cache_lifetime'));

    return $results;
  }

  /**
   * Builds and returns an array of sort options, keyed by method.
   *
   * @param bool $full
   *   (optional) Set this to TRUE if you want to get all of the supported sort
   *   methods. Defaults to FALSE.
   *
   * @return array
   *   An array of sort options, keyed by method.
   */
  protected function getSortOptions($full = FALSE) {
    $sort_options = array(
      'score' => array('method' => 'score', 'name' => t('Relevancy'), 'default_sort' => 'desc'),
      'usage' => array('method' => 'usage', 'name' => t('Most installed'), 'default_sort' => 'desc'),
      'title' => array('method' => 'title', 'name' => t('Title'), 'default_sort' => 'asc'),
      'name' => array('method' => 'name', 'name' => t('Author'), 'default_sort' => 'asc'),
      'latest_release' => array('method' => 'latest_release', 'name' => t('Latest release'), 'default_sort' => 'desc'),
    );

    if ($full) {
      $sort_options['type'] = array(
        'method' => 'type',
        'name' => t('Type'),
        'default_sort' => 'asc'
      );
      $sort_options['created'] = array(
        'method' => 'created',
        'name' => t('Date created'),
        'default_sort' => 'asc'
      );
      $sort_options['latest_activity'] = array(
        'method' => 'latest_activity',
        'name' => t('Latest build'),
        'default_sort' => 'desc'
      );
    }

    return $sort_options;
  }

  /**
   * Builds a themed widget to select the server.
   *
   * This is only called if there are more than one server enabled in the
   * settings.
   *
   * @param array $servers
   *   An array of servers that should be available as options.
   * @param string $current_server
   *   The currently selected server, which is a string.
   *
   * @return string
   *   A themed server select widget, as a string.
   */
  protected function getServerWidget(array $servers, $current_server) {
    $list = array();
    $list[] = array(
      'data' => t('Repository:'),
      'class' => array('server-header')
    );
    $current_path = current_path();

    // @todo Improve comment or variable name of $i.
    // Interate through the repository number and current server number.
    $i = 0;

    foreach ($servers as $url => $server) {
      $classes = array();
      $query = array(
        'repository' => $i,
      );

      // If the sort option is currently active, handle it differently.
      if ($current_server == $i) {
        $classes[] = 'server-active';
      }
      else {
        $classes[] = 'server-inactive';
      }

      $list[] = array(
        '#type' => 'link',
        '#title' => $server['name'],
        '#href' => $current_path,
        '#options' => array('query' => $query, 'class' => array()),
        '#wrapper_attributes' => array(
          'class' => $classes,
        ),
      );

      $i += 1;
    }

    // @todo Should return a render array.
    return array(
      '#theme' => 'item_list',
      '#items' => $list,
      '#type' => 'ul',
      '#attributes' => array('class' => array('project-browser-servers-list')),
    );
  }

  /**
   * Builds a themed sort widget for the results.
   *
   * These are links which can be clicked/toggled to select and change direction.
   *
   * @param array $sort_options
   *   An array of sort options.
   * @param string $current_sort_option
   *   The currently selected sort option, which is a string.
   * @param string $current_sort_direction
   *   The currently selected sort direction, which is a string.
   *
   * @return string
   *   A themed list of sort options, as a string.
   */
  protected function getSortWidget(array $sort_options, $current_sort_option, $current_sort_direction) {
    $sort_list = array();
    $sort_list[] = array(
      '#markup' => t('Sort by:'),
      '#wrapper_attributes' => array(
        'class' => array('sort-header'),
      ),
    );

    foreach ($sort_options as $sort_option) {
      $classes = array();
      $query = array(
        'order_by' => $sort_option['method'],
        'sort' => $sort_option['default_sort'],
      );

      // If the sort option is currently active, handle it differently.
      if ($current_sort_option == $sort_option['method']) {
        $classes[] = 'sort-active';
        $classes[] = 'sort-' . $current_sort_direction;

        // Set the direction of the sort link to the opposite of what it currently
        // is.
        if ($current_sort_direction == $query['sort']) {
          if ($query['sort'] == 'desc') {
            $query['sort'] = 'asc';
          }
          else {
            $query['sort'] = 'desc';
          }
        }
      }
      else {
        $classes[] = 'sort-inactive';
      }
      $sort_list[] = array(
        '#type' => 'link',
        '#title' => $sort_option['name'],
        '#href' => current_path(),
        '#options' => array('query' => $query, 'class' => array()),
        '#wrapper_attributes' => array(
          'class' => $classes,
        ),
      );
    }

    // @todo Should return a render array.
    return array(
      '#theme' => 'item_list',
      '#items' => $sort_list,
      '#type' => 'ul',
      '#attributes' => array('class' => array('project-browser-sort-list')),
    );
  }
}