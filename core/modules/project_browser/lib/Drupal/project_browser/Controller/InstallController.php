<?php

/**
 * @file
 * Contains \Drupal\project_browser\Controller\InstallController.
 */

namespace Drupal\project_browser\Controller;

use Drupal\Core\DependencyInjection\ContainerInjectionInterface;
use Drupal\node\NodeInterface;
use Symfony\Component\DependencyInjection\Container;
use Symfony\Component\DependencyInjection\ContainerInterface;
use Symfony\Component\HttpFoundation\Response;
use Symfony\Component\HttpKernel\Exception\NotFoundHttpException;

/**
 * Controller routines for book routes.
 */
class InstallController implements ContainerInjectionInterface {

  /**
   * Constructs a BookController object.
   *
   * @param \Drupal\book\BookManager $bookManager
   *   The book manager.
   * @param \Drupal\book\BookExport $bookExport
   *   The book export service.
   */
  public function __construct() {
  }

  /**
   * {@inheritdoc}
   */
  public static function create(ContainerInterface $container) {
    return new static();
  }

  public function adminInstall($project) {
    return array('foo' => array(
      '#type' => 'markup',
      '#markup' => 'Hello world install',
    ));
  }

  public function queue($first, $third, $fourth) {
    return array('foo' => array(
      '#type' => 'markup',
      '#markup' => 'Hello world queue',
    ));
  }
}