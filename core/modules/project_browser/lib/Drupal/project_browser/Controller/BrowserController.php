<?php

/**
 * @file
 * Contains \Drupal\project_browser\Controller\BrowserController.
 */

namespace Drupal\project_browser\Controller;

use Drupal\Core\Config\Config;
use Drupal\Core\DependencyInjection\ContainerInjectionInterface;
use Drupal\project_browser\ProjectBrowserBase;
use Guzzle\Http\Exception;
use Symfony\Component\DependencyInjection\Container;
use Symfony\Component\DependencyInjection\ContainerInterface;
use Symfony\Component\HttpFoundation\Response;
use Symfony\Component\HttpKernel\Exception\NotFoundHttpException;

/**
 * Controller routines for book routes.
 */
class BrowserController extends ProjectBrowserBase implements ContainerInjectionInterface {

  /**
   * {@inheritdoc}
   */
  public static function create(ContainerInterface $container) {
    return new static(
      $container->get('config.factory')->get('project_browser.settings')
    );
  }

  protected function adminBrowseProjects() {
    // Set any filters in the session that are needed.
    if (!empty($_GET['repository'])) {
      $_SESSION['project_browser_server_filter'] = $_GET['repository'];
    }
    if (!empty($_GET['order_by'])) {
      $_SESSION['project_browser_order_by_filter_' . $this->projectType] = $_GET['order_by'];
    }
    if (!empty($_GET['sort'])) {
      $_SESSION['project_browser_sort_filter_' . $this->projectType] = $_GET['sort'];
    }

    // Build the filters.
    $drupal_version = explode('.', \Drupal::CORE_COMPATIBILITY);
    $filters = array(
      'version' => $drupal_version[0],
      'type' => $this->projectType,
    );

    // Add filters.
    if (isset($_SESSION['project_browser_category_filter_' . $this->projectType])) {
      $categories = array_filter($_SESSION['project_browser_category_filter_' . $this->projectType]);
      if (!empty($categories)) {
        $filters['categories'] = $this->prepareCategories($categories, $this->projectType);
      }
    }
    if (isset($_SESSION['project_browser_text_filter_' . $this->projectType])) {
      $filters['text'] = $_SESSION['project_browser_text_filter_' . $this->projectType];
    }
    if (isset($_SESSION['project_browser_order_by_filter_' . $this->projectType])) {
      $filters['order_by'] = $_SESSION['project_browser_order_by_filter_' . $this->projectType];
    }
    if (isset($_SESSION['project_browser_sort_filter_' . $this->projectType])) {
      $filters['sort'] = $_SESSION['project_browser_sort_filter_' . $this->projectType];
    }
    if (isset($_SESSION['project_browser_server_filter'])) {
      $filters['server'] = $_SESSION['project_browser_server_filter'];
    }
    else {
      $filters['server'] = 0;
    }
    $filters['requested'] = 10;
    $filters['page'] = isset($_GET['page']) ? $_GET['page'] : 0;

    // Get the projects to display here based on the filters.
    $results = $this->fetchResults($filters);

    // Save the listed projects in the session so it can be used.
    $_SESSION['project_browser_listed_projects'] = $results['projects'];

    if (isset($_SESSION['project_browser_listed_projects'])) {
      $test = $_SESSION['project_browser_listed_projects'];
    }

    $list = array();
    foreach ($results['projects'] as $project) {
      $list[] = $project;
    }

    // Add the pager.
    $total = $results['total'];
    $num_per_page = 10;
    $page = pager_default_initialize($total, $num_per_page);
    $offset = $num_per_page * $page;
    $start = ($total) ? $offset + 1 : 0;
    $finish = $offset + $num_per_page;
    if ($finish > $total) {
      $finish = $total;
    }

    $sort_options = $this->getSortOptions();
    $current_order_by = isset($_SESSION['project_browser_order_by_filter_' . $this->projectType]) ? $_SESSION['project_browser_order_by_filter_' . $this->projectType] : 'score';
    $current_sort = isset($_SESSION['project_browser_sort_filter_' . $this->projectType]) ? $_SESSION['project_browser_sort_filter_' . $this->projectType] : 'desc';

    $build = array();
    $build['content'] = array(
      'project_browser_header' => array(
        '#markup' => t('Showing @start to @finish of @total.', array(
          '@start' => $start, '@finish' => $finish, '@total' => $total)),
        '#weight' => 0,
      ),
      'project_browser_sort_header' => array(
        '#type' => 'item',
        '#weight' => 2,
        '#markup' => $this->getSortWidget($sort_options, $current_order_by, $current_sort),
      ),
      'project_browser_list' => array(
        '#markup' => theme('project_browser_list', array('projects_list' => $list, 'type' => $this->projectType)),
        '#weight' => 3,
      ),
      'pager' => array(
        '#theme' => 'pager',
        '#weight' => 99,
      ),
    );
    $servers = $this->getServers();

    if (count($servers) > 1) {
      $build['content']['project_browser_server_header'] = array(
        '#type' => 'item',
        '#weight' => 1,
        '#markup' => $this->getServerWidget($servers, $filters['server']),
      );
    }

    $build['#attached']['library'][] = array(
      'project_browser',
      'drupal.project_browser'
    );

    return $build;
  }
  public function adminBrowseModules() {
    $this->projectType = 'modules';
    return $this->adminBrowseProjects();
  }

  public function adminBrowseThemes() {
    $this->projectType = 'themes';
    return $this->adminBrowseProjects();
   }
}
