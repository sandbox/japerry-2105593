<?php
namespace Drupal\project_browser\Form;

use Drupal\Core\Form\FormInterface;
use Drupal\project_browser\ProjectBrowserBase;
use Drupal\Core\Config\Config;

/**
* Provides a test form object.
*/
class ProjectFiltersForm extends ProjectBrowserBase implements FormInterface {

  public function __construct($project_type) {
    $this->config = \Drupal::config('project_browser.settings');
    $this->projectType = $project_type;
  }

  /**
   * {@inheritdoc}
   */
  public function getFormID() {
    return 'project_browser_filters_form';
  }

  /**
   * Form constructor for the filters form.
   *
   * This includes categories and the string search box, and the $type is stored.
   *
   * @param string $type
   *   The type of project (module or theme).
   *
   * @see project_browser_filters_form_submit()
   * @ingroup forms
   */
  public function buildForm(array $form, array &$form_state) {
    $form['search_text'] = array(
      '#type' => 'textfield',
      '#size' => '25',
      '#title' => t('Search String'),
      '#default_value' => isset($_SESSION['project_browser_text_filter_' . $this->projectType]) ? $_SESSION['project_browser_text_filter_' . $this->projectType] : '',
    );

    // Add the category filter if there are categories.
    if ($categories = $this->getCategories()) {
      array_unshift($categories, t('-- Any --'));
      $form['category'] = array(
        '#type' => 'select',
        '#title' => t('Category'),
        '#options' => $categories,
        '#default_value' => isset($_SESSION['project_browser_category_filter_' . $this->projectType]) ? $_SESSION['project_browser_category_filter_' . $this->projectType] : NULL,
      );
    }

    $form['project_type'] = array(
      '#type' => 'value',
      '#value' => $this->projectType,
    );

    $form['submit'] = array(
      '#type' => 'submit',
      '#value' => t('Filter'),
    );

    return $form;
  }
  /**
   * {@inheritdoc}
   */
  public function validateForm(array &$form, array &$form_state) {
    drupal_set_message(t('The FormTestObject::validateForm() method was used for this form.'));
  }

  /**
   * Form submission handler for project_browser_filters_form().
   *
   * All that we do here is store the selected category and search string in the
   * $_SESSION variable.
   */
  public function submitForm(array &$form, array &$form_state) {
    $type = $form_state['values']['project_type'];
    if (!empty($form_state['values']['category'])) {
      $_SESSION['project_browser_category_filter_' . $type] = array($form_state['values']['category'] => $form_state['values']['category']);
    }
    else {
      $_SESSION['project_browser_category_filter_' . $type] = array();
    }
    $_SESSION['project_browser_text_filter_' . $type] = $form_state['values']['search_text'];
  }
}