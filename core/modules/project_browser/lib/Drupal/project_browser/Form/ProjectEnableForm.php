<?php
namespace Drupal\project_browser\Form;

use Drupal\Core\Form\FormInterface;
use Drupal\project_browser\ProjectBrowserBase;

/**
* Provides a test form object.
*/
class ProjectEnableForm extends ProjectBrowserBase implements FormInterface {

  public function __construct($projects) {
    $this->projects = $projects;
  }
  /**
   * {@inheritdoc}
   */
  public function getFormID() {
    return 'project_browser_installation_select_versions_form';
  }

  /**
   * {@inheritdoc}
   */
  public function buildForm(array $form, array &$form_state) {
    $modules = system_rebuild_module_data();
    $form['instructions'] = array(
      '#type' => 'item',
      '#markup' => t('The projects you selected have been successfully installed.  If you installed any new modules, you may enable them using the form below or on the main !link page.', array('!link' => l(t('Modules'), 'admin/modules'))),
    );

    $options = array();
    $missing = array();

    foreach ($this->projects as $project) {
      if ($project['type'] == 'module') {
        $dependency_check = TRUE;
        $dependencies = array();
        if (isset($modules[$project['name']])) {
          foreach ($modules[$project['name']]->info['dependencies'] as $dependency) {
            if (isset($modules[$dependency])) {
              $dependencies[] = $modules[$dependency]->info['name'] . ' (' . t('Installed') . ')';
            }
            else {
              $dependency_check = FALSE;
              $dependencies[] = $dependency . ' (' . t('Missing') . ')';
            }
          }
          if ($dependency_check) {
            $options[$project['name']] = array(
              array('data' => $modules[$project['name']]->info['name']),
              array('data' => $modules[$project['name']]->info['version']),
              array('data' => implode(', ', $dependencies)),
            );
          }
          else {
            $missing[$project['name']] = array(
              array('data' => $modules[$project['name']]->info['name']),
              array('data' => $modules[$project['name']]->info['version']),
              array('data' => implode(', ', $dependencies)),
            );
          }
        }
        else {
          drupal_set_message(t('There was an error getting information for @module', array('@module' => $project['name'])), 'error');
        }
      }
    }

    $headers = array(
      array('data' => t('Title')),
      array('data' => t('Version')),
      array('data' => t('Dependencies')),
    );

    if (!empty($options)) {
      $form['modules'] = array(
        '#type' => 'tableselect',
        '#title' => t('Enable modules'),
        '#description' => t('Select which modules you would like to enable.'),
        '#header' => $headers,
        '#options' => $options,
        '#empty' => t('No new modules installed.'),
        '#multiple' => TRUE,
        '#js_select' => TRUE,
        '#weight' => 1,
      );

      $form['submit'] = array(
        '#type' => 'submit',
        '#submit' => array('project_browser_installation_enable_form_submit'),
        '#value' => t('Enable modules'),
        '#weight' => 99,
      );
    }

    if (!empty($missing)) {
      $form['missing'] = array(
        '#type' => 'item',
        '#title' => t('Missing Dependencies'),
        '#description' => t('These modules are missing one or more dependencies, and so cannot be enabled.'),
        '#markup' => theme('table', array('header' => $headers, 'rows' => $missing)),
        '#weight' => 2,
      );
    }

    return $form;
  }

  /**
   * {@inheritdoc}
   */
  public function validateForm(array &$form, array &$form_state) {
    drupal_set_message(t('The FormTestObject::validateForm() method was used for this form.'));
  }

  /**
   * {@inheritdoc}
   */
  public function submitForm(array &$form, array &$form_state) {
    drupal_set_message(t('The FormTestObject::submitForm() method was used for this form.'));
    Drupal::config('form_test.object')
      ->set('bananas', $form_state['values']['bananas'])
      ->save();
  }

}