<?php

/**
 * @file
 * Contains \Drupal\project_browser\Form\FileSystemForm.
 */

namespace Drupal\project_browser\Form;

use Drupal\Core\Config\ConfigFactory;
use Drupal\Core\Config\Context\ContextInterface;
use Drupal\Core\KeyValueStore\KeyValueStoreInterface;
use Drupal\Core\Form\ConfigFormBase;
use Symfony\Component\DependencyInjection\ContainerInterface;
use Symfony\Component\HttpFoundation\RedirectResponse;

/**
 * Configure file system settings for this site.
 */
class DevelopmentSettingsForm extends ConfigFormBase {

  /**
   * Stores the state storage service.
   *
   * @var \Drupal\Core\KeyValueStore\KeyValueStoreInterface
   */
  protected $state;

  /**
   * Constructs a CronForm object.
   *
   * @param \Drupal\Core\Config\ConfigFactory $config_factory
   *   The factory for configuration objects.
   * @param \Drupal\Core\Config\Context\ContextInterface $context
   *   The configuration context used for this configuration object.
   * @param \Drupal\Core\KeyValueStore\KeyValueStoreInterface $state
   *   The state key value store.
   */
  public function __construct(ConfigFactory $config_factory, ContextInterface $context, KeyValueStoreInterface $state) {
    parent::__construct($config_factory, $context);
    $this->state = $state;
  }

  /**
   * {@inheritdoc}
   */
  public static function create(ContainerInterface $container) {
    return new static(
      $container->get('config.factory'),
      $container->get('config.context.free'),
      $container->get('state')
    );
  }

  /**
   * {@inheritdoc}
   */
  public function getFormID() {
    return 'project_browser_development_settings';
  }

  /**
   * {@inheritdoc}
   */
  public function buildForm(array $form, array &$form_state) {
    $config = $this->configFactory->get('project_browser.settings');
    $form['main'] = array(
      '#type' => 'details',
      '#title' => t('Main settings'),
      '#collapsed' => FALSE,
    );
    // Because this is a pluggable system, there can be other repositories besides
    // Drupal.org.
    $form['main']['server_list'] = array(
      '#type' => 'textarea',
      '#title' => t('Repositories'),
      '#default_value' => $config->get('server.list'),
      '#description' => t("Add new repositories to use for the Project Browser, one per line, in the 'url|method|Site Name' format. Drupal.org is added by default, and doesn't need to be set here."),
      '#required' => FALSE,
    );

    return parent::buildForm($form, $form_state);
  }

  /**
   * {@inheritdoc}
   */
  public function submitForm(array &$form, array &$form_state) {
    $config = $this->configFactory->get('project_browser.settings');
    $config->set('server.list', $form_state['values']['server_list']);
    $config->save();

    parent::submitForm($form, $form_state);
  }

}
