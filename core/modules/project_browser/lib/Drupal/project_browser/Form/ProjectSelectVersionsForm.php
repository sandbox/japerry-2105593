<?php
namespace Drupal\project_browser\Form;

use Drupal\Core\Form\FormInterface;

/**
* Provides a test form object.
*/
class ProjectSelectVersionsForm implements FormInterface {
  protected $projects;

  public function __construct($projects) {
    $this->projects = $projects;
  }

  /**
   * {@inheritdoc}
   */
  public function getFormID() {
    return 'project_browser_installation_select_versions_form';
  }

  /**
   * Form constructor for the select versions form.
   *
   * @param array $projects
   *   An array of projects to get the releases for.
   *
   * @see project_browser_installation_select_versions_form_submit()
   * @ingroup forms
   */
  public function buildForm(array $form, array &$form_state) {
    module_load_include('inc', 'project_browser', 'project_browser');
    drupal_add_library('project_browser', 'drupal.project_browser.select_releases');

    $form = array();

    // First unset any old data.
    unset($_SESSION['project_browser_install_releases_list']);

    $form['#tree'] = TRUE;

    $form['releases-header'] = array(
      '#type' => 'item',
      '#markup' => t("You're about to install:"),
    );

    $form['releases'] = array();

    foreach ($this->projects as $project) {
      // Get the available releases for this project.
      if (!$release_data = project_browser_get_project_release_data($project)) {
        drupal_set_message(t('Could not fetch releases for project %project.', array('%project' => $project['title'])), 'warning');
        watchdog('project_browser', 'Could not fetch releases for project %project.', array('%project' => $project['title']), WATCHDOG_ERROR);
        project_browser_install_queue_remove($project['name']);
        continue;
      }

      // We use the update module to calculate the recommended version.
      $project_data = array(
        'existing_major' => 0,
        'existing_version' => 0,
        'install_type' => '',
      );
      module_load_include('inc', 'update', 'update.compare');
      update_calculate_project_update_status($project_data, $release_data);

      $releases_list = array();

      foreach ($release_data['releases'] as $version => $release) {
        $release_title = t("@project @version - @date", array(
          '@project' => $project['title'],
          '@version' => $release['version'],
          '@date' => format_date($release['date'], 'custom', 'M j, Y'),
          ));
        if (isset($release['terms']['Release type']) AND !empty($release['terms']['Release type'])) {
          $release_title .= " (" . implode(', ', $release['terms']['Release type']) . ")";
        }
        if (isset($release['release_link'])) {
          $releases_list[$version] = l($release_title, $release['release_link']);
        }
        else {
          $releases_list[$version] = $release_title;
        }
      }

      $form['releases'][$project['name']]['project'] = array(
        '#type' => 'value',
        '#value' => $project,
      );

      $form['releases'][$project['name']]['release_name'] = array(
        '#type' => 'radios',
        '#title' => t('Select release for @project', array('@project' => $project['title'])),
        '#options' => $releases_list,
        '#default_value' => key($releases_list),
        '#prefix' => '<div class="project-browser-releases-wrapper project-browser-release-' . $project['name'] . '">',
        '#suffix' => '</div>',
        '#attributes' => array(
          'class' => array('project-browser-releases-radios'),
        ),
        '#required' => TRUE,
      );
      $form['releases'][$project['name']]['selected_text'] = array(
        '#type' => 'item',
        '#prefix' => '<div class="project-browser-selected-release project-browser-selected-release-' . $project['name'] . '">',
        '#suffix' => '</div>',
        '#markup' => reset($releases_list),
      );
      if (isset($project_data['recommended'])) {
        // If there is a recommended release set, then only show it and show the
        // jQuery link.
        $recommended_releases = array();
        $recommended_releases[$project_data['recommended']] = $releases_list[$project_data['recommended']];
        $form['releases'][$project['name']]['release_name']['#default_value'] = $project_data['recommended'];
        $form['releases'][$project['name']]['selected_text']['#markup'] = $releases_list[$project_data['recommended']];
      }
      if (count($releases_list) > 1) {
        $form['releases'][$project['name']]['selected_text']['#markup'] .= " (<span class='project-browser-show-releases-link' rel='" . $project['name'] . "'>" . t('change release') . "</span>)";
      }
    }

    // If there is nothing to install, go to the enable page.
    if (empty($form['releases'])) {
      drupal_set_message(t('No releases data found for any of the selected projects.'), 'warning');
      drupal_goto('admin/modules/project-browser/install/enable');
    }

    $form['backup_warning'] = array(
      '#type' => 'markup',
      '#markup' => t('Back up your database and site before you continue. !link.', array('!link' => l(t('Learn how'), 'http://drupal.org/node/22281'))),
    );
    $form['maintenance_mode'] = array(
      '#type' => 'checkbox',
      '#title' => t('Perform updates with site in maintenance mode (strongly recommended)'),
      '#default_value' => TRUE,
    );

    $form['submit'] = array(
      '#type' => 'submit',
      '#value' => t('Install'),
    );

    return $form;
  }

  /**
   * {@inheritdoc}
   */
  public function validateForm(array &$form, array &$form_state) {
    drupal_set_message(t('The FormTestObject::validateForm() method was used for this form.'));
  }

  /**
   * Form submission handler for project_browser_installation_select_versions_form().
   *
   * This sets the batch to install the different selected releases one by one.
   */
  public function submitForm(array &$form, array &$form_state) {
    module_load_include('inc', 'project_browser', 'project_browser');
    // Store maintenance_mode setting so we can restore it when done.
    $_SESSION['maintenance_mode'] = config('system.maintenance')->get('enabled');
    if ($form_state['values']['maintenance_mode'] == TRUE) {
      config('system.maintenance')->set('enabled', TRUE)->save();
    }

    foreach ($form_state['values']['releases'] as $item) {
      // Load the selected release.
      if ($release = project_browser_get_release($item['release_name'], $item['project'])) {
        // Add the release to a session variable.
        $_SESSION['project_browser_install_releases_list'][$item['project']['name']] = array(
          'release_name' => $item['release_name'],
          'project' => $item['project'],
        );
      }
    }

    // Install the projects with batch.
    module_load_include('inc', 'update', 'update.manager');

    $queued_releases = project_browser_get_queued_releases();

    $operations = array();
    foreach ($queued_releases as $short_name => $info) {
      $operations[] = array('_project_browser_batch_install_release', array($info['release_name'], $info['project']));
    }
    $batch = array(
      'operations' => $operations,
      'finished' => '_project_browser_batch_install_releases_finished',
      'title' => t('Installing projects'),
      'init_message' => t('Installing modules...'),
      'progress_message' => t('Installed @current out of @total.'),
      'error_message' => t('Installation has encountered an error.'),
      'file' => drupal_get_path('module', 'project_browser') . '/project_browser.inc',
    );
    batch_set($batch);
  }

}
?>